provider "aws" {
  region                  = var.my_region
  shared_credentials_file = "~/.aws/credentials"
}
terraform {
  required_version = ">= 1.0.0"
}
module "wordpress_module" {
  source                        = "../modules/wordpress"
  depends_on                    = [module.networking]
  my_instance_type              = var.my_instance_type
  my_key_name                   = var.my_key_name
  wp_instance_name              = var.wp_instance_name
  vpc_security_group_ids_output = module.networking.security_group
  subnet_id_output              = module.networking.subnet_id
}

module "networking" {
  source                 = "../modules/networking"
  vpc_cidr_block         = var.vpc_cidr_block
  is_enable_dns_hostname = var.is_enable_dns_hostname
  instance_tenancy       = var.instance_tenancy
  vpc_name               = var.vpc_name
  igw_name               = var.igw_name
  rt_igw_cidr_block      = var.rt_igw_cidr_block
  route_table_name       = var.route_table_name
  public_sn_cidr_block   = var.public_sn_cidr_block
  az_subnets             = var.az_subnets
  public_sn_name         = var.public_sn_name
  private_sn_cidr_block  = var.private_sn_cidr_block
  private_sn_name        = var.private_sn_name
  security_group_name    = var.security_group_name
  rds_sg_name            = var.rds_sg_name
  nat_gw_name            = var.nat_gw_name
}

module "rds_module" {
  source     = "terraform-aws-modules/rds/aws"
  depends_on = [module.networking]
  version    = "~> 3.0"

  identifier = "demodb"

  engine            = "mysql"
  engine_version    = "5.7.19"
  instance_class    = "db.t2.large"
  allocated_storage = 5

  name     = "wordpress"
  username = "user"
  password = "mywordpass"
  port     = "3306"

  iam_database_authentication_enabled = true

  vpc_security_group_ids = [module.networking.rds_sg]

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  tags = {
    Owner       = "user"
    Environment = "dev"
  }

  # DB subnet group
  subnet_ids = [ module.networking.private_subnet, module.networking.private_subnet_2]

  # DB parameter group
  family = "mysql5.7"

  # DB option group
  major_engine_version = "5.7"

  # Database Deletion Protection
  deletion_protection = false

  parameters = [
    {
      name  = "character_set_client"
      value = "utf8mb4"
    },
    {
      name  = "character_set_server"
      value = "utf8mb4"
    }
  ]

  options = [
    {
      option_name = "MARIADB_AUDIT_PLUGIN"

      option_settings = [
        {
          name  = "SERVER_AUDIT_EVENTS"
          value = "CONNECT"
        },
        {
          name  = "SERVER_AUDIT_FILE_ROTATIONS"
          value = "37"
        },
      ]
    },
  ]

}
## wordpress vars
variable "my_instance_type" {
  type        = string
  default     = "t2.micro"
  description = "my instance type."
}

variable "my_key_name" {
  type        = string
  default     = "summaryKP"
  description = "the name of my key value."
}

variable "wp_instance_name" {
  type        = string
  default     = "Wordpress_instance"
  description = "my wordpress instance name."
}

## vars for the output from networking module to wordpress
variable "vpc_security_group_ids_output" {
  type        = string
  default     = "sg-0479e7736f4392791"
  description = "the vpc sg id, token from net module"
}

variable "subnet_id_output" {
  type        = string
  default     = "random_sub"
  description = "value"
}



#8. Creating word press instance
resource "aws_instance" "wb_server1" {
  ami                         = data.aws_ssm_parameter.this.value
  instance_type               = var.my_instance_type
  key_name                    = var.my_key_name
  subnet_id                   = var.subnet_id_output
  vpc_security_group_ids      = [var.vpc_security_group_ids_output]
  associate_public_ip_address = true

  user_data = <<-EOF
        #!/bin/bash
        sudo yum -y update
        sudo yum install -y httpd
        sudo amazon-linux-extras install -y lamp-mariadb10.2-php7.2 php7.2
        wget https://wordpress.org/latest.tar.gz
        tar -xzf latest.tar.gz
        sudo cp -r wordpress/* /var/www/html/
        sudo chown -R apache:apache /var/www/html
        sudo systemctl start httpd && sudo systemctl enable httpd
        sudo systemctl start php-fpm && sudo systemctl enable php-fpm
  EOF

  tags = {
    Name = var.wp_instance_name
  }
}





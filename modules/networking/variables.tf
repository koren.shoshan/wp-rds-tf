#networks vars
variable "vpc_cidr_block" {
  type        = string
  default     = "10.0.0.0/16"
  description = "the cidr block of vpc, usually class B."
}

variable "is_enable_dns_hostname" {
  type        = bool
  default     = true
  description = "define if dns hostname is enable."
}

variable "instance_tenancy" {
  type        = string
  default     = "default"
  description = "Tenancy of instances spin up within VPC."
}

variable "vpc_name" {
  type        = string
  default     = "my_default_vcp"
  description = "vpc name."
}

variable "igw_name" {
  type        = string
  default     = "my_default_vpc"
  description = "igw name."
}

variable "rt_igw_cidr_block" {
  type        = string
  default     = "0.0.0.0/0"
  description = "the route table igw cidr"
}

variable "route_table_name" {
  type        = string
  default     = "default_route_table"
  description = "the raute table name."
}

variable "public_sn_cidr_block" {
  type        = string
  default     = "10.0.3.0/24"
  description = "cider block of public subnet."
}

variable "az_subnets" {
  type        = string
  default     = "us-east-1a"
  description = "The az of subnets."
}

variable "public_sn_name" {
  type        = string
  default     = "public_subnet"
  description = "the public subnet name."
}

variable "private_sn_cidr_block" {
  type        = string
  default     = "10.0.1.0/24"
  description = "cider block of private subnet."
}

variable "private_sn_name" {
  type        = string
  default     = "private_sn"
  description = "the public subnet name."
}

## second private subnet for rds

variable "private_sn_cidr_block_2" {
  type        = string
  default     = "10.0.2.0/24"
  description = "cider block of second private subnet."
}
## second az for rds
variable "az_subnet_2" {
  type        = string
  default     = "us-east-1b"
  description = "The az of rds."
}
variable "security_group_name" {
  type    = string
  default = "wordpress_sg"
}

variable "rds_sg_name" {
  type    = string
  default = "rds_sg"
}
variable "nat_gw_name" {
  type    = string
  default = "gw_NAT_private"
}

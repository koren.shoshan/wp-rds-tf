# 1. Creat a VPC
resource "aws_vpc" "main_vpc" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_hostnames = var.is_enable_dns_hostname
  instance_tenancy     = "default"

  tags = {
    Name = var.vpc_name
  }
}

#2. Create an IGW
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main_vpc.id

  tags = {
    Name = var.igw_name
  }
}

# 3. Create custom route table- mention the vpc & connect it to IGW

resource "aws_route_table" "route-table-igw" {
  vpc_id = aws_vpc.main_vpc.id

  route {
    cidr_block = var.rt_igw_cidr_block
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = var.route_table_name
  }
}

#4.Create 1 Subnets- in order to place wordpress instances into them- take care diffrent cidr IP's.

resource "aws_subnet" "public_sn" {
  vpc_id            = aws_vpc.main_vpc.id
  cidr_block        = var.public_sn_cidr_block
  availability_zone = var.az_subnets

  tags = {
    Name = var.public_sn_name
  }
}

#4.2 Create private subnet

resource "aws_subnet" "private_sn" {
  vpc_id            = aws_vpc.main_vpc.id
  cidr_block        = var.private_sn_cidr_block
  availability_zone = var.az_subnets

  tags = {
    Name = var.private_sn_name
  }
}

#4.3 Create second private subnet

resource "aws_subnet" "private_sn_for_rds" {
  vpc_id            = aws_vpc.main_vpc.id
  cidr_block        = var.private_sn_cidr_block_2
  availability_zone = var.az_subnet_2

  tags = {
    Name = var.private_sn_name
  }
}


#5. Associate the route table with IGW (instead of two subnets).
resource "aws_main_route_table_association" "app-igw-route" {
  vpc_id         = aws_vpc.main_vpc.id
  route_table_id = aws_route_table.route-table-igw.id
}

#6. Create Security group to allow port 22,80 

resource "aws_security_group" "allow_web" {
  name        = "allow_web_traffic"
  description = "Allow web traffic inbound traffic"
  vpc_id      = aws_vpc.main_vpc.id

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] #better to use my private ip
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1" #means i can take outside anywhere
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.security_group_name
  }
}

#6.2 TO DB 3306

resource "aws_security_group" "allow_rds" {
  name        = "allow_rds_traffic"
  description = "Allow web traffic inbound traffic"
  vpc_id      = aws_vpc.main_vpc.id

  ingress {
    description = "MYSQL"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    security_groups = [ aws_security_group.allow_web.id ] #better to use my private ip
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1" #means i can take outside anywhere
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.rds_sg_name
  }
}

#7. Nat-Gateway - associate with the private sn.

resource "aws_nat_gateway" "ng-private" {
  connectivity_type = "private"
  subnet_id         = aws_subnet.private_sn.id

  tags = {
    Name = var.nat_gw_name
  }

}
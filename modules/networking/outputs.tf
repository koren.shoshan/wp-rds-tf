output "security_group" {
  value = aws_security_group.allow_web.id
}

output "subnet_id" {
  value = aws_subnet.public_sn.id
}

output "private_subnet" {
  value = aws_subnet.private_sn.id
}

output "private_subnet_2" {
  value = aws_subnet.private_sn_for_rds.id
}

output "rds_sg" {
  value = aws_security_group.allow_rds.id
}